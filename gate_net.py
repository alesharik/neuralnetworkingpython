from keras.models import Sequential
from keras.layers import Dense, Activation
import numpy as np


def generate_model(input_neurons, layers, hidden_neurons):
    model = Sequential()
    model.add(Dense(units=hidden_neurons, input_dim=input_neurons, use_bias=True))
    model.add(Activation("relu"))
    for _ in range(layers):
        model.add(Dense(hidden_neurons, use_bias=False))
        model.add(Activation("relu"))
    model.add(Dense(units=1, use_bias=False))
    model.add(Activation("relu"))
    return model


if __name__ == '__main__':
    print("Enter input count:")
    input_neurons = int(input())
    print("Enter layer count:")
    layers = int(input())
    print("Enter hidden neuron count:")
    hidden_neurons = int(input())
    model = generate_model(input_neurons, layers, hidden_neurons)
    model.compile(optimizer="rmsprop", loss="mean_absolute_error", metrics=["accuracy"])

    in_data = []
    out_data = []
    print("Enter data count:")
    data_count = int(input())
    for _ in range(data_count):
        dat = []
        for i in range(input_neurons):
            print("V{}:".format(i))
            dat.append(int(input()))
        print("R:")
        out = int(input())
        in_data.append(dat)
        out_data.append([out])
    print("Enter epoch count:")
    epochs = int(input())
    model.fit(x=np.array(in_data), y=np.array(out_data), epochs=epochs, batch_size=len(out_data))

    print("Complete!")
    while True:
        dat = []
        print("Enter values")
        for i in range(input_neurons):
            print("V{}:".format(i))
            dat.append(int(input()))
        out = model.predict(np.array([dat]))
        print("Output: {}".format(out[0][0]))
